import React from 'react';
import { Menu } from 'semantic-ui-react';
import { NavLink } from 'react-router-dom';

const NavBar = () => {
  return (
    <Menu>
      <Menu.Item as={NavLink} exact activeClassName="active-nav-link" to="/">Home</Menu.Item>
      <Menu.Item as={NavLink} exact activeClassName="active-nav-link" to="/user">User</Menu.Item>
      <Menu.Item as={NavLink} exact activeClassName="active-nav-link" to="/admin">Admin</Menu.Item>
      <Menu.Item as={NavLink} exact activeClassName="active-nav-link" to="/package">Package</Menu.Item>
      <Menu.Item as={NavLink} exact activeClassName="active-nav-link" to="/dashbaord">Dashboard</Menu.Item>
    </Menu>
  );
};

export default NavBar;
