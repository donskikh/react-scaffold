import React from 'react';
import { Switch, Route } from 'react-router-dom';
import * as containers from '../';
import NavBar from './NavBar';

import 'normalize.css';
import 'semantic-ui-css/semantic.min.css';
import './App.scss';
import logo from '../../logo.svg';

const App = () => {
  return (
    <div className="app-root">
      <NavBar />
      <header className="app-header">
        <img src={logo} className="app-logo" alt="logo" />
        <h1 className="app-title">Welcome to NPA</h1>
      </header>
      <main>
        <Switch>
          <Route exact path="/" component={containers.Home} />
          <Route exact path="/user" component={containers.User} />
          <Route exact path="/admin" component={containers.Admin} />
          <Route path="/package" component={containers.Package} />
          <Route path="/dashbaord" component={containers.Dashboard} />
          <Route component={containers.NoMatch404} />
        </Switch>
      </main>
      <footer>Luxoft&copy; 2017</footer>
    </div>
  );
};

export default App;
