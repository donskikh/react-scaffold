import React from 'react';
import { shallow } from 'enzyme';
import { PackageList } from '../PackageList';

it('renders without crashing', () => {
  const initialState = {
    hasErrored: true,
    isLoading: true,
    packages: [{
      id: '1',
      createdAt: 1508241216,
      name: 'name 1',
      imageUrl: 'imageUrl 1',
      version: 28
    }],
    fetchData: () => {}
  };

  shallow(<PackageList {...initialState} />);
});
