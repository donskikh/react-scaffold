import React from 'react';
import { shallow } from 'enzyme';
import PackageListItem from '../PackageListItem';

it('renders without crashing', () => {
  const testPackageItem = {
    id: '1',
    createdAt: 1508241216,
    name: 'name 1',
    imageUrl: 'imageUrl 1',
    version: 28
  };
  shallow(<PackageListItem packageItem={testPackageItem} />);
});
