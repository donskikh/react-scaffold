import React from 'react';
import './Package.scss';
import PackageList from './PackageList';

const Package = () => {
  return (
    <section className="package-root">
      <h2>Package page</h2>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam corporis aspernatur
        error quod nam quos aut impedit aperiam nulla, a totam reiciendis repellat dignissimos
        eos quae cumque eaque neque culpa?
      </p>
      <PackageList />
    </section>
  );
};

export default Package;
