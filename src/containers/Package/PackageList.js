import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

import { List, Loader } from 'semantic-ui-react';
import { packagesFetchData } from '../../redux/actions/packageActions';
import { PACKAGES_ENDPOINT_URL } from '../../redux/constants/packageConstants';
import PackageListItem from './PackageListItem';

class PackageList extends Component {
  componentDidMount() {
    this.props.fetchData(PACKAGES_ENDPOINT_URL);
  }

  render() {
    if (this.props.hasErrored) {
      return <p>Sorry! Error while loading.</p>;
    } else if (this.props.isLoading) {
      return <Loader active inline="centered" content="Loading" />;
    }
    return (
      <List>
        {this.props.packages.map(item => {
          return <PackageListItem key={item.id} packageItem={item} />;
        })}
      </List>
    );
  }
}

const mapStateToProps = state => {
  return {
    packages: state.packages,
    hasErrored: state.packagesHasErrored,
    isLoading: state.packagesIsLoading
  };
};

const mapDistapchToProps = dispatch => {
  return {
    fetchData: url => dispatch(packagesFetchData(url))
  };
};

PackageList.propTypes = {
  fetchData: PropTypes.func.isRequired,
  hasErrored: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  packages: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    createdAt: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    version: PropTypes.number.isRequired
  })).isRequired
};

export { PackageList }; // For the testing purposes

export default connect(mapStateToProps, mapDistapchToProps)(PackageList);
