import React from 'react';
import PropTypes from 'prop-types';
import { Icon, List } from 'semantic-ui-react';
import './Package.scss';

const PackageListItem = props => {
  const {
    packageItem: {
      name,
      createdAt,
      version
    }
  } = props;

  const createdDate = new Date(createdAt)
    .toString()
    .split(' ')
    .splice(0, 4)
    .join(' ');

  return (
    <List.Item>
      <Icon name="book" />
      <List.Content>
        <List.Header>{name}</List.Header>
        <List.Description>
          Created at: {createdDate}. Version: {version}
        </List.Description>
      </List.Content>
    </List.Item>
  );
};

PackageListItem.propTypes = {
  packageItem: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    createdAt: PropTypes.number.isRequired,
    version: PropTypes.number.isRequired
  }).isRequired
};

export default PackageListItem;
