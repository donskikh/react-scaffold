import React from 'react';
import PropTypes from 'prop-types';
import './NoMatch404.scss';

const NoMatch404 = ({ location }) => {
  return (
    <section className="no-match-404-root">
      <h2>404</h2>
      <h3>Sorry, no match for {location.pathname}</h3>
    </section>
  );
};

NoMatch404.propTypes = {
  location: PropTypes.string.isRequired
};

export default NoMatch404;
