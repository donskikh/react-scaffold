import React from 'react';
import { shallow } from 'enzyme';
import NoMatch404 from './NoMatch404';

it('renders without crashing', () => {
  shallow(<NoMatch404 location="test-location" />);
});
