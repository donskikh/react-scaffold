import React from 'react';
import './Dashboard.scss';

const Dashboard = () => {
  return (
    <section className="dashboard-root">
      <h2>Dashboard page</h2>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam corporis aspernatur
        error quod nam quos aut impedit aperiam nulla, a totam reiciendis repellat dignissimos
        eos quae cumque eaque neque culpa?
      </p>
    </section>
  );
};

export default Dashboard;
