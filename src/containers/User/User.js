import React from 'react';
import './User.scss';

const User = () => {
  return (
    <section className="user-root">
      <h2>User page</h2>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam corporis aspernatur
        error quod nam quos aut impedit aperiam nulla, a totam reiciendis repellat dignissimos
        eos quae cumque eaque neque culpa?
      </p>
    </section>
  );
};

export default User;
