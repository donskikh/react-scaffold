import App from './App/App';
import Admin from './Admin/Admin';
import Dashboard from './Dashboard/Dashboard';
import Home from './Home/Home';

import Package from './Package/Package';
import User from './User/User';
import NoMatch404 from './NoMatch404/NoMatch404';

export {
  App,
  Admin,
  Dashboard,
  Home,
  Package,
  User,
  NoMatch404
};
