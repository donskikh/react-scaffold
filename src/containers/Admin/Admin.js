import React from 'react';
import './Admin.scss';

const Admin = () => {
  return (
    <section className="admin-root">
      <h2>Admin page</h2>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam corporis aspernatur
        error quod nam quos aut impedit aperiam nulla, a totam reiciendis repellat dignissimos
        eos quae cumque eaque neque culpa?
      </p>
    </section>
  );
};

export default Admin;
