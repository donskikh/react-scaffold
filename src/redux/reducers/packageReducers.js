import { PACKAGES_ACTION_TYPES } from '../constants/packageConstants';

export function packagesHasErrored(state = false, action) {
  if (action.type === PACKAGES_ACTION_TYPES.HAS_ERRORED) {
    return action.hasErrored;
  }
  return state;
}

export function packagesIsLoading(state = false, action) {
  if (action.type === PACKAGES_ACTION_TYPES.IS_LOADING) {
    return action.isLoading;
  }
  return state;
}

export function packages(state = [], action) {
  if (action.type === PACKAGES_ACTION_TYPES.FETCH_DATA_SUCCESS) {
    return action.packages;
  }
  return state;
}
