import { combineReducers } from 'redux';
import {
  packages,
  packagesHasErrored,
  packagesIsLoading
} from './packageReducers';

export default combineReducers({
  packages,
  packagesIsLoading,
  packagesHasErrored
});
