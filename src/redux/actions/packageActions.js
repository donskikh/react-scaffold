import 'whatwg-fetch';
import { PACKAGES_ACTION_TYPES } from '../constants/packageConstants';

export function packagesHasErrorred(bool) {
  return {
    type: PACKAGES_ACTION_TYPES.HAS_ERRORED,
    hasErrored: bool
  };
}

export function packagesIsLoading(bool) {
  return {
    type: PACKAGES_ACTION_TYPES.IS_LOADING,
    isLoading: bool
  };
}

export function packagesFetchDataSuccess(packages) {
  return {
    type: PACKAGES_ACTION_TYPES.FETCH_DATA_SUCCESS,
    packages
  };
}

export function packagesFetchData(url) {
  return dispatch => {
    dispatch(packagesIsLoading(true));

    fetch(url)
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText);
        } else {
          dispatch(packagesIsLoading(false));
        }
        return response;
      })
      .then(response => response.json())
      .then(packages => dispatch(packagesFetchDataSuccess(packages)))
      .catch(() => {
        dispatch(packagesHasErrorred(true));
      });
  };
}
