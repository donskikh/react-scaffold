import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import configureStore from './redux/configureStore';
import registerServiceWorker from './registerServiceWorker';
import { App } from './containers';

const store = configureStore();

ReactDOM.render(
  (
    <Provider store={store}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Provider>
  )
  , document.getElementById('root')
);

if (module.hot) {
  module.hot.accept();
}

registerServiceWorker();
