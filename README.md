# Front-end scaffold

This scaffold is created base on Facebook's native create-react-app project with a few improvements. 100% ready-to-go, develop or deploy with zero additional configuration.

Improvements list:   
- [Enzyme](https://github.com/airbnb/enzyme) for testing   
- [Router](https://github.com/ReactTraining/react-router) v4   
- [Redux](https://github.com/reactjs/redux) state management approach   
- [Thunk](https://github.com/gaearon/redux-thunk) middlaware   
- Hot Module Reloading   
- [Normalize](https://necolas.github.io/normalize.css/).css   
- [Precss](https://github.com/jonathantneal/precss) PostCss module (allows to use Sass-like syntax)   
- [Ramda](http://ramdajs.com/) for the functional programming approach   
- [Semantic UI](https://react.semantic-ui.com/introduction) library. Special React edition for modern, consistent UI   
- Strict [EsLint](https://github.com/airbnb/javascript) config from AirBnB for the static code checking   
- [Dockerfile](https://docs.docker.com/) to preserve the same environment for all developers / QA / BA

## Conventions
* Project sctucture, building and configuration described in [README_SCAFFOLD](./README_SCAFFOLD.md)
* React best practives and code style guide described in [REACT_STYLE_GUIDE](./REACT_STYLE_GUIDE.md)

## Unit-testing
* Please cover all new code with unit-tests. All examples are existed in the project
* If you got only one file, add **.test.js** suffix, otherway - add \__tests__ (two underscores before and after) directory and use the same name for test file as original (no suffix needed). Example: './src/container/Package/__tests__

## How to start / build / test

### Run development server and start app   
#### Start App
```npm start```

#### Run tests
```npm test```   
For CI:   
```CI=true npm test```   
With Coverage report   
```GENERATE_SOURCEMAP=true npm test```

#### Create build for production ('./build' directory)
```GENERATE_SOURCEMAP=false NODE_ENV="production" npm run build```   
You can run it with:   
```serve -s build```

## Git

For bracnching use [Git-flow](https://danielkummer.github.io/git-flow-cheatsheet/index.ru_RU.html) approach.   
For Commit messages, please follow the next few rules:
0) Start form Jira ticket number: 'NPA-15 Create documentation for the new scaffold'
1) Separate subject from body with a blank line   
2) Limit the subject line to 50 characters   
3) Capitalize the subject line   
4) Do not end the subject line with a period   
5) Use the imperative mood in the subject line   
6) Wrap the body at 72 characters   
7) Use the body to explain what and why vs. how   

[Details and why this matters](https://chris.beams.io/posts/git-commit/)